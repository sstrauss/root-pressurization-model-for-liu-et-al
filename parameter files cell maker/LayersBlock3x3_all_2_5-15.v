// Make a block of 3D plant tissue
// Coordinate system (x,y,z) or ...
//                   (a,r,l) (angle, radius, length) for cylindrical

[Main]
Debug: 0
Epsilon: .00001
SegSz: 2 2 2 // size of segments

// The next section defines the cell layers, Go Wild!
// These are the cell sizes in segments
LayerSegs: 8 48 8
LayerSegs: 8 64 8
LayerSegs: 8 80 8
LayerSegs: 8 96 8
LayerSegs: 8 120 8

// Starting points of cells in segments
LayerStart: 0 458 0
LayerStart: 0 606 0
LayerStart: 0 802 0
LayerStart: 0 1046 0
LayerStart: 0 1342 0

// Ending points of cells in segments
LayerEnd: 24 602 24
LayerEnd: 24 798 24
LayerEnd: 24 1042 24
LayerEnd: 24 1334 24
LayerEnd: 24 1702 24

// Make layers or not (good for debugging)
LayerMake: true
LayerMake: true
LayerMake: true
LayerMake: true
LayerMake: true
