// Make a block of 3D plant tissue
// Coordinate system (x,y,z) or ...
//                   (a,r,l) (angle, radius, length) for cylindrical

[Main]
Debug: 0
Epsilon: .00001
SegSz: 2 2 2 // size of segments

// The next section defines the cell layers, Go Wild!
// These are the cell sizes in segments
LayerSegs: 8 4 8
LayerSegs: 8 6 8
LayerSegs: 8 8 8
LayerSegs: 8 12 8
LayerSegs: 8 16 8
LayerSegs: 8 24 8
LayerSegs: 8 32 8

// Starting points of cells in segments
LayerStart: 0 0 0
LayerStart: 0 16 0
LayerStart: 0 38 0
LayerStart: 0 66 0
LayerStart: 0 106 0
LayerStart: 0 158 0
LayerStart: 0 234 0

// Ending points of cells in segments
LayerEnd: 24 12 24
LayerEnd: 24 34 24
LayerEnd: 24 62 24
LayerEnd: 24 102 24
LayerEnd: 24 154 24
LayerEnd: 24 230 24
LayerEnd: 24 330 24

// Make layers or not (good for debugging)
LayerMake: true
LayerMake: true
LayerMake: true
LayerMake: true
LayerMake: true
LayerMake: true
LayerMake: true
