// Make a cylindrical 3D plant tissue
// Coordinate system (x,y,z) or ...
//                   (a,r,l) (angle, radius, length) for cylindrical

[Main]
Debug: 0
Epsilon: .00001
SegSz: 40 2 36 // size of segments in (a,r,l) a is # of divisions of the circle.

// The next section defines the cell layers, Go Wild!
// Coordinates (a,r,l) = (angle, radius, length)
// Here we are defining the layers from the inside out
// These are the cell sizes in segments
LayerSegs: 10 8 5 // Steele 
LayerSegs: 5 5 5 // Endodermis
LayerSegs: 5 8 5 // Cortex
LayerSegs: 2 9 5 // Epidermis

// Starting points of cells in segments
LayerStart: 0 0 0 // Steele
LayerStart: 4 8 2 // Endodermis
LayerStart: 2 13 0 // Cortex
LayerStart: 0 21 2 // Epidermis

// Ending points of cells in segments
LayerEnd: 40 8 202 // Steele
LayerEnd: 44 13 202 // Endodermis
LayerEnd: 42 21 202 // Cortex
LayerEnd: 40 30 202 // Epidermis

// Stagger of every other cell within a layer (l-dir)
Stagger: 0 // Steele
Stagger: 2 // Endodermis
Stagger: 2 // Cortex
Stagger: 2 // Epidermis

// Make layers or not (good for debugging)
LayerMake: true
LayerMake: true
LayerMake: true
LayerMake: true
